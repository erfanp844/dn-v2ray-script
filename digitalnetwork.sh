#!/usr/bin/env bash

# Install.sh file for install DigitalNetwork-Core and dependency.
# Update server and install base dependency (Docker ,CURL , git , Wget , acme).
# Config all service with this installer , get certificate and add to
# cron tab for renew before expire.

set -e

if [ -z "$APP_NAME" ]; then
    APP_NAME="digitalnetwork"
fi

if [ -z "$APP_DIR" ]; then
    APP_DIR="/opt/$APP_NAME"
fi

if [ -z "$DATA_DIR" ]; then
    DATA_DIR="/var/lib/$APP_NAME"
fi

if [ -z "$SCRIPT_DATA_DIR" ]; then
    SCRIPT_DATA_DIR="/var/lib/$APP_NAME-script"
fi

if [ -z "$SCRIPT_REPO_NAME" ]; then
    SCRIPT_REPO_NAME="erfanp844/dn-v2ray-script"
fi

if [ -z "$COMPOSE_FILE" ]; then
    COMPOSE_FILE="$APP_DIR/docker-compose.yml"
fi

if [ -z "$GITLAB_PROJECT_ID" ]; then
    GITLAB_PROJECT_ID=53329565
fi

if [ -z "$GITLAB_API_ENDPOINT" ]; then
    GITLAB_API_ENDPOINT="https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID"
fi

colorized_echo() {
    local color=$1
    local text=$2

    case $color in
        "red")
        printf "\e[91m${text}\e[0m\n";;
        "green")
        printf "\e[92m${text}\e[0m\n";;
        "yellow")
        printf "\e[93m${text}\e[0m\n";;
        "blue")
        printf "\e[94m${text}\e[0m\n";;
        "magenta")
        printf "\e[95m${text}\e[0m\n";;
        "cyan")
        printf "\e[96m${text}\e[0m\n";;
        *)
            echo "${text}"
        ;;
    esac
}

validate_input() {
  local success_function=$1
  local prompt_message=$2
  local regex_pattern=$3
  local input_name=$4
  local placeholder=$5

  read -p "${prompt_message} (e.g., ${placeholder}): " input

  if [[ $input =~ $regex_pattern ]]; then
    $success_function "$input"
    return 0  # Return code 0 for success
  else
    colorized_echo red "Incorrect $input_name, please enter a valid $input_name."
    return 1  # Return code 1 for failure
  fi
}

ask_yes_no_strict() {
    local question=$1
    local response

    while true; do
        read -p "$question [y/n]: " response
        case "$response" in
            [yY][eE][sS]|[yY])
                return 0
                ;;
            [nN][oO]|[nN])
                return 1
                ;;
            *)
                colorized_echo red "Please respond with 'y' or 'n'."
                ;;
        esac
    done
}

check_running_as_root() {
    if [ "$(id -u)" != "0" ]; then
        colorized_echo red "This command must be run as root."
        exit 1
    fi
}

detect_os() {
    # Detect the operating system
    if [ -f /etc/lsb-release ]; then
        OS=$(lsb_release -si)
        elif [ -f /etc/os-release ]; then
        OS=$(awk -F= '/^NAME/{print $2}' /etc/os-release | tr -d '"')
        elif [ -f /etc/redhat-release ]; then
        OS=$(cat /etc/redhat-release | awk '{print $1}')
        elif [ -f /etc/arch-release ]; then
        OS="Arch"
    else
        colorized_echo red "Unsupported operating system"
        exit 1
    fi
}

detect_and_update_package_manager() {
    colorized_echo blue "Updating package manager"
    if [[ "$OS" == "Ubuntu"* ]] || [[ "$OS" == "Debian"* ]]; then
        PKG_MANAGER="apt-get"
        $PKG_MANAGER update
        elif [[ "$OS" == "CentOS"* ]]; then
        PKG_MANAGER="yum"
        $PKG_MANAGER update -y
        $PKG_MANAGER epel-release -y
        elif [[ "$OS" == "Fedora"* ]]; then
        PKG_MANAGER="dnf"
        $PKG_MANAGER update
        elif [ "$OS" == "Arch" ]; then
        PKG_MANAGER="pacman"
        $PKG_MANAGER -Sy
    else
        colorized_echo red "Unsupported operating system"
        exit 1
    fi
}

detect_compose() {
    # Check if docker compose command exists
    if docker compose >/dev/null 2>&1; then
        COMPOSE='docker compose'
        elif docker-compose >/dev/null 2>&1; then
        COMPOSE='docker-compose'
    else
        colorized_echo red "docker compose not found"
        exit 1
    fi
}

detect_script_env() {
  if ! [ -f "$SCRIPT_DATA_DIR/.env" ]; then
    colorized_echo red "Script env not found."
  fi
  source "$SCRIPT_DATA_DIR/.env"
}

is_digitalnetwork_installed() {
    if [ -d $APP_DIR ]; then
        return 0
    else
        return 1
    fi
}

is_digitalnetwork_script_installed() {
    if [ -f "$SCRIPT_DATA_DIR/.env" ]; then
        return 0
    else
        return 1
    fi
}

ban_iranian_service() {

  if is_digitalnetwork_installed; then

    # Detect envirment variable script
    detect_script_env

    # Make Xray Assets path
    local MARZBAN_ASSETS="$DATA_DIR/marzban/assets/"
    mkdir -p "$MARZBAN_ASSETS"

    # Download Xray Assets
    colorized_echo blue "Fetching geosite and geoip file..."
    curl --silent "$GEOSITE_FILE" -o "$MARZBAN_ASSETS/geosite.dat"
    curl --silent "$GEOIP_FILE" -o "$MARZBAN_ASSETS/geoip.dat"
    curl --silent "$GEIRAN_FILE" -o "$MARZBAN_ASSETS/iran.dat"
    colorized_echo green "Fetch successfully"

    #Add Xray Assets Path to marzban.env
    sed -i "s~\(XRAY_ASSETS_PATH = \).*~\1'$MARZBAN_ASSETS'~" "$APP_DIR/marzban.env"
    colorized_echo green "Update Marzban env"

    colorized_echo blue "Fetching routing setting for ban Iranian service"
    curl --silent "$IR_BAN_REPO_RAW/routing.json" -o "$DATA_DIR/routing.json"
    colorized_echo green "Fetch successfully"

    jq --argfile routingFile "$DATA_DIR/routing.json" '.routing = $routingFile.routing' "$DATA_DIR/marzban/xray_config.json" > tmp_xray_config.json
    mv tmp_xray_config.json "$DATA_DIR/marzban/xray_config.json"
    rm "$DATA_DIR/routing.json"
    # Change tag of last object in outbounds (if protocol blackhole)
    jq '(.outbounds[] | select(.protocol == "blackhole")).tag = "blackhole"' "$DATA_DIR/marzban/xray_config.json" > tmp_xray_config.json
    mv tmp_xray_config.json "$DATA_DIR/marzban/xray_config.json"
    colorized_echo green "Ban Iranian service is successfully"

  else
    colorized_echo red "digitalnetwork and marzban app not install yet, Please install and ban Iranian service."
  fi
}

install_cloudfalar_warp() {
  # use colorized echo red and msg : cloudfalar wrap not added to this script version
  colorized_echo blue "Installing Cloudfalar warp...."
  colorized_echo red "Cloudfalar is deprecated on this script version"

}

setup_domian(){

    success_domain() {
      local domain=$1
      colorized_echo green "Congratulations! The Domian is correct."
      # Add any additional code to execute upon successful validation of Domain

      sed -i "/^export DOMAIN=/{s/example\.com/$domain/}" "$SCRIPT_DATA_DIR/.env"
    }

    domain_regex="^[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)\.[a-zA-Z]{2,7}$"
    while ! validate_input success_domain "Please enter a Domain" "$domain_regex" "Domain" "sub.example.com"; do
      : # If validation fails, prompt the user again
    done
}

setup_marzban(){

    if ask_yes_no_strict "Do you want to ban Iranian domains and IPs?"; then
        colorized_echo green "YES!!! , OK prepring to ban Iranian domains and IP ..."
        ban_iranian_service
    else
        colorized_echo yellow "No!!! , This decision of yours can cause the VPN server to be blocked ..."
    fi

    if ask_yes_no_strict "Do you want to install Cloudfalar warp for google openAi and etc ...?"; then
        colorized_echo green "YES!!! , OK prepring to enable Cloudfalar warp ..."
        install_cloudfalar_warp
    else
        colorized_echo yellow "No!!! , This decision of yours can cause the VPN server to be blocked ..."
    fi


}

setup_haproxy(){

    # success reality tcp sni function
    success_reality_tcp_sni() {
      local SNI=$1
      colorized_echo green "Congratulations! The SNI is correct."

      # Search and replace REALITY_TCP_SNI in the script .env file

      sed -i "/^export REALITY_TCP_SNI=/{s/reality_tcp.com/$SNI/}" "$SCRIPT_DATA_DIR/.env"
    }

    # success reality grpc sni function

    success_reality_grpc_sni() {
      local SNI=$1
      colorized_echo green "Congratulations! The Reality"

      # Search and replace REALITY_GRPC_SNI in the script .env file

      sed -i "/^export REALITY_GRPC_SNI=/{s/reality_grpc.com/$SNI/}" "$SCRIPT_DATA_DIR/.env"
    }

    sni_regex="^[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)\.[a-zA-Z]{2,7}$"

    while ! validate_input success_reality_tcp_sni "Please enter a Reality tcp sni" "$sni_regex" "SNI" "sni.example.com"; do
      : # If validation fails, prompt the user again
    done

    while ! validate_input success_reality_grpc_sni "Please enter a Reality grpc sni" "$sni_regex" "SNI" "sni.example.com"; do
      : # If validation fails, prompt the user again
    done
}

setup_CF_toke_and_email(){

    success_cf_token() {
      local cf_key=$1
      colorized_echo green "Congratulations! The CF Token is correct."
      # Add any additional code to execute upon successful validation of CF Token

      sed -i "s/CF_Key=fffffffffffffffffffffffffffffffffffff/CF_Key=$cf_key/" "$SCRIPT_DATA_DIR/.env"
    }

    success_cf_email() {
      local cf_email=$1
      colorized_echo green "Congratulations! The CF Email is correct."
      # Add any additional code to execute upon successful validation of CF Email

      sed -i "s/CF_Email=example@gmail\.com/CF_Email=$cf_email/" "$SCRIPT_DATA_DIR/.env"
    }

    handle_success_all_info(){
      colorized_echo blue "Check CF Token and CF Email online for genrate certificate"
    }


    cf_token_regex="^[0-9a-fA-F]{37}$"
    while ! validate_input success_cf_token "Please enter CloudFalar Global Token (CF Token)" "$cf_token_regex" "CF Token" "abc123"; do
      : # If validation fails, prompt the user again
    done


    cf_email_regex="^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$"
    while ! validate_input success_cf_email "Please enter CloudFalar Email" "$cf_email_regex" "CF Email" "user@example.com"; do
      : # If validation fails, prompt the user again
    done


    handle_success_all_info
}

setup_gitlab_access_token() {

    curlf() {
      OUTPUT_FILE=$(mktemp)
      HTTP_CODE=$(curl --silent --output "$OUTPUT_FILE" --write-out "%{http_code}" "$@")
      EXIT_CODE=$((HTTP_CODE))
      rm "$OUTPUT_FILE"
    }

    check_online_validation(){

        ACCESS_TOKEN=$1

        curlf --header "PRIVATE-TOKEN: $ACCESS_TOKEN" "$GITLAB_API_ENDPOINT/"

        if [ $EXIT_CODE -ne 200 ]; then
          # Authentication failed error 401 exit code 22

          colorized_echo red "Authentication failed , your access key incorrect ,
Please enter again! ($EXIT_CODE)"

          gitlab_access_token_validation

        elif [ $EXIT_CODE -eq 200 ]; then
          # Add Access Key to env file and export

          colorized_echo green "gitlab access token set is successfully : $ACCESS_TOKEN"
          export GITLAB_ACCESS_TOKEN=$ACCESS_TOKEN
          sed -i "/^export GITLAB_ACCESS_TOKEN=/{s/gitlab_access_token_value/$ACCESS_TOKEN/}" "$SCRIPT_DATA_DIR/.env"
        fi
    }

    gitlab_access_token_validation(){
      while ! validate_input check_online_validation "Please enter you're gitlab access token to install digitalnetwork Core : " "^glpat-[A-Za-z0-9_-]{20}$" "Gitlab Access Token" "glpat-AAAAAAAAAAAAAAAAAAAA"; do
        : # If validation fails, prompt the user again
      done
    }

    gitlab_access_token_validation
}

fetch_gitlab_raw_file() {

  if [[ ! $GITLAB_ACCESS_TOKEN =~ ^glpat-[A-Za-z0-9_-]{20}$ ]]; then
      colorized_echo red "Gitlab access token is invalid!"
      exit 1
  fi

  local HTTP_STATUS_CODE
  local FILE_PATH=$(echo "$1" | jq -Rr '@uri')
  local OUTPUT_FILE=$2

  HTTP_STATUS_CODE=$(curl --write-out "%{http_code}" --header "PRIVATE-TOKEN: $GITLAB_ACCESS_TOKEN" "$GITLAB_API_ENDPOINT/repository/files/$FILE_PATH/raw?ref=main" -o "$OUTPUT_FILE")

  # Conver HttpCode string to integer
  HTTP_STATUS_CODE=$((HTTP_STATUS_CODE))


  if [ $HTTP_STATUS_CODE -ne 200 ]; then
    colorized_echo red "Fetching error with exit code : $EXIT_CODE"
    exit 1
  fi

}

preparing_Marzban() {

  # Make marzban dir for store a marzban data app
  mkdir -p "$DATA_DIR/marzban"
  local MARZBAN_DATA_DIR="$DATA_DIR/marzban"

  colorized_echo blue "Fetching marzban env"
  fetch_gitlab_raw_file "marzban.env" "$APP_DIR/marzban.env"
  sed -i 's/^# \(XRAY_JSON = .*\)$/\1/' "$APP_DIR/marzban.env"
  sed -i 's/^# \(SQLALCHEMY_DATABASE_URL = .*\)$/\1/' "$APP_DIR/marzban.env"
  sed -i 's~\(XRAY_JSON = \).*~\1"/var/lib/marzban/xray_config.json"~' "$APP_DIR/marzban.env"
  sed -i 's~\(SQLALCHEMY_DATABASE_URL = \).*~\1"sqlite:////var/lib/marzban/db.sqlite3"~' "$APP_DIR/marzban.env"
  colorized_echo green "File saved in $APP_DIR/marzban.env"

  colorized_echo blue "Fetching xray config file"
  fetch_gitlab_raw_file "services/marzban/xray_config.json" "$MARZBAN_DATA_DIR/xray_config.json"
  colorized_echo green "File saved in $MARZBAN_DATA_DIR/xray_config.json"
}

preparing_HaProxy() {

  # Make haproxy dir for store a haproxy data app
  mkdir -p "$DATA_DIR/haproxy"
  local HAPROXY_DATA_DIR="$DATA_DIR/haproxy"

  detect_script_env

  colorized_echo blue "Fetching haproxy.cfg file"
  fetch_gitlab_raw_file "services/haproxy/haproxy.cfg" "$HAPROXY_DATA_DIR/haproxy.cfg"
  sed -i "s/panel.example.com/$DOMAIN/g" "$HAPROXY_DATA_DIR/haproxy.cfg"
  sed -i "s/reality\_tcp\.com/$REALITY_TCP_SNI/g" "$HAPROXY_DATA_DIR/haproxy.cfg"
  sed -i "s/reality\_grpc\.com/$REALITY_GRPC_SNI/g" "$HAPROXY_DATA_DIR/haproxy.cfg"
  colorized_echo green "File saved in $HAPROXY_DATA_DIR/haproxy.cfg"
}

install_package () {

    if [ -z $PKG_MANAGER ]; then
        detect_and_update_package_manager
    fi

    PACKAGE=$1
    colorized_echo blue "Installing $PACKAGE"
    if [[ "$OS" == "Ubuntu"* ]] || [[ "$OS" == "Debian"* ]]; then
        $PKG_MANAGER -y install "$PACKAGE"
        elif [[ "$OS" == "CentOS"* ]]; then
        $PKG_MANAGER install -y "$PACKAGE"
        elif [[ "$OS" == "Fedora"* ]]; then
        $PKG_MANAGER install -y "$PACKAGE"
        elif [ "$OS" == "Arch" ]; then
        $PKG_MANAGER -S --noconfirm "$PACKAGE"
    else
        colorized_echo red "Unsupported operating system"
        exit 1
    fi
}

install_docker() {
    # Install Docker and Docker Compose using the official installation script
    colorized_echo blue "Installing Docker"
    curl -fsSL https://get.docker.com | sh
    colorized_echo green "Docker installed successfully"
}

install_acme() {
    # Install Acme using the official installation script
    colorized_echo blue "Installing Acme"
    curl -fsSL https://get.acme.sh | sh
    colorized_echo green "Acme installed successfully"
}

install_digitalnetwork_script() {
    REPO_RAW_URL="https://gitlab.com/$SCRIPT_REPO_NAME/raw/main"
    colorized_echo blue "Installing digitalnetwork script"
    curl -sSL "$REPO_RAW_URL/digitalnetwork.sh" | install -m 755 /dev/stdin /usr/local/bin/$APP_NAME
    mkdir -p "$SCRIPT_DATA_DIR"
    curl -sL "$REPO_RAW_URL/.env.example" -o "$SCRIPT_DATA_DIR/.env"
    detect_script_env
    colorized_echo green "digitalnetwork script installed successfully"
}

install_digitalnetwork_core() {

    # Make directory fro app an data
    mkdir -p "$DATA_DIR"
    mkdir -p "$APP_DIR"

    # Fetch Compose File
    colorized_echo blue "Fetching compose file"
    fetch_gitlab_raw_file "docker-compose.yaml" "$APP_DIR/docker-compose.yaml"
    colorized_echo green "File saved in $APP_DIR/docker-compose.yaml"

    # Fetching .env file
    colorized_echo blue "Fetching .env file"
    fetch_gitlab_raw_file ".env" "$APP_DIR/.env"
    colorized_echo green "File saved in $APP_DIR/.env"

    # Preparing Marzban
    colorized_echo yellow "Preparing Marzban"
    preparing_Marzban
    setup_marzban
    colorized_echo green "Marzban is ready for run $APP_NAME"

    # Preparing HaProxy
    colorized_echo yellow "Preparing HaProxy"
    setup_haproxy
    preparing_HaProxy
    colorized_echo green "HaProxy is ready for run $APP_NAME"

    colorized_echo green "$APP_NAME files downloaded successfully"
}

install_command() {
    check_running_as_root
    # Check if digitalnetwork is already installed
    if is_digitalnetwork_installed; then
        colorized_echo red "DigitalNetwork is already installed at $APP_DIR"
        read -p "Do you want to override the previous installation? (y/n) "
        if [[ ! $REPLY =~ ^[Yy]$ ]]; then
            colorized_echo red "Aborted installation"
            exit 1
        fi
    fi
    detect_os
    if ! command -v jq >/dev/null 2>&1; then
        install_package jq
    fi
    if ! command -v curl >/dev/null 2>&1; then
        install_package curl
    fi
    if ! command -v socat >/dev/null 2>&1; then
        install_package socat
    fi
    if ! command -v ~/.acme.sh/acme.sh >/dev/null 2>&1; then
        install_acme
    fi
    if ! command -v docker >/dev/null 2>&1; then
        install_docker
    fi
    detect_compose
    install_digitalnetwork_script
    setup_gitlab_access_token
    setup_domian
    install_digitalnetwork_core

    #up_digitalnetwork_core
    #follow_digitalnetwork_logs
}

usage() {
    colorized_echo red "Usage: $APP_NAME [command]"
    echo
    echo "Commands:"
    echo "  up          Start services"
    echo "  down        Stop services"
    echo "  restart     Restart services"
    echo "  status      Show status"
    echo "  logs        Show logs"
    echo "  cli         DigitalNetwork CLI"
    echo "  install     Install DigitalNetwork Core"
    echo "  update      Update latest version DigitalNetwork Core"
    echo "  uninstall   Uninstall DigitalNetwork Core"
    echo
}

#if is_digitalnetwork_script_installed; then
#  detect_script_env
#fi

case "$1" in
    up)
    shift; up_command "$@";;
    down)
    shift; down_command "$@";;
    restart)
    shift; restart_command "$@";;
    status)
    shift; status_command "$@";;
    logs)
    shift; logs_command "$@";;
    cli)
    shift; cli_command "$@";;
    install)
    shift; install_command "$@";;
    update)
    shift; update_command "$@";;
    uninstall)
    shift; uninstall_command "$@";;
    *)
    usage;;
esac
